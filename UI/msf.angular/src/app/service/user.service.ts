import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl='https://localhost:44324/api/User';
  constructor(private http:HttpClient) { }
  getAll():Observable<User[]>{
    return this.http.get<User[]>(this.baseUrl);
  }
  create(data: any): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }
  deleteUser(id:string):Observable<User>{
    return this.http.delete<User>(this.baseUrl+'/'+id);
  }
  getById(id:string):Observable<User>{
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }
  update(id:string,data:any):Observable<any>{
    return this.http.put<any>(`${this.baseUrl}/${id}`, data);
  }
  
}
