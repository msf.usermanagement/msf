import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  title='user';
  users:User[]=[];
  user: User={
    ID:'',
    Name:'',
    Email:'',
    Password:''
  };
  constructor(private userService : UserService) { }

  ngOnInit(): void {
    this.getAll();
  }
getAll(){
  this.userService.getAll().subscribe(
    res=>{
      this.users=res;
    }
  );
}

deleteUser(id:string){
  if(confirm('Are you sure?')){
    this.userService.deleteUser(id).subscribe(res=>{
      this.getAll();
    })
  }
}

detailUser(user:User){
  this.user=user;
}
}
