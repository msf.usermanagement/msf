import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  
  constructor(private userService:UserService, private route:ActivatedRoute, private router:Router) { }
   
  user: User={
    ID:'',
    Name:'',
    Email:'',
    Password:''
  };
  mes='';
  public userRecord?:User[];
  ngOnInit(): void {
    this.mes='';
    this.getUser(this.route.snapshot.params['id']);
  }

  onSubmit(id:string){
   if(id!=null){
    this.updateUser();
   }
    this.createUser();
  }
  getUser(id:string):void{
    this.userService.getById(id).subscribe(
      res=>{
        this.user=res;
        console.log(res);
      },
      err=>{
        console.log(err);
      }
    );
  }
  createUser():void{
    const data = {
      Id:this.user.ID,
      Name:this.user.Name,
      Email:this.user.Email,
      Password:this.user.Password
    };

    this.userService.create(data)
      .subscribe(
        response => {
          console.log(response);
          
        },
        error => {
          console.log(error);
        });
  }
  updateUser(): void {
    this.mes = '';

    this.userService.update(this.user.ID, this.user)
      .subscribe(
        response => {
          console.log(response);
        
        },
        error => {
          console.log(error);
        });
  }
}
