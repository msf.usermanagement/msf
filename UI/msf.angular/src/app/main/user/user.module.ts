import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from "@angular/router";
import { UserComponent } from './user.component';
import { UserFormComponent } from './user-form/user-form.component';

const userRoutes:Routes=[
  {path:'' ,component:UserComponent,pathMatch:'full'},
  {path:'create',component:UserFormComponent},
  {path:'edit/:id' ,component:UserFormComponent},
  {path:'detail/:id',component:UserFormComponent}
]
@NgModule({
  declarations: [
    UserFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
    FormsModule
    
  ]
})
export class UserModule { }
