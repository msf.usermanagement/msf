import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { mainRoutes } from './main-routing.module';

import { RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { UserComponent } from './user/user.component';



@NgModule({
  declarations: [MainComponent, UserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(mainRoutes)
    
  ]
})
export class MainModule { }
